<?php
/**
 * Created by PhpStorm.
 * User: hip
 * Date: 25/08/15
 * Time: 10:04
 */

use \Mockery as m;
use POSD\Persistence\PaymentRepository;
use GuzzleHttp\Psr7;


class ResponseProcessorTest extends \PHPUnit_Framework_TestCase {

    public function setUp()
    {
        parent::setUp();
    }
    public function tearDown()
    {
        m::close();
    }

    /**
     * @test
     */
    public function it_marks_response_with_200_as_success()
    {
        $stream = Psr7\stream_for('{"data" : "test"}');
        $response = new GuzzleHttp\Psr7\Response(200, ['Content-Type' => 'application/json'], $stream);
        $this->repo = m::mock(PaymentRepository::class)->shouldReceive('markSuccess')->once()->getMock();

        $processor = new \POSD\Handlers\ResponseProcessor($this->repo);
        $processor->process($response,m::mock(\POSD\Persistence\Payment::class));

    }

    /**
     * @test
     */
    public function it_marks_response_with_400_as_invalid_json()
    {
        $stream = Psr7\stream_for('{"data" : "test"}');
        $response = new GuzzleHttp\Psr7\Response(400, ['Content-Type' => 'application/json'], $stream);
        $this->repo = m::mock(PaymentRepository::class)->shouldReceive('markInvalid')->once()->getMock();

        $processor = new \POSD\Handlers\ResponseProcessor($this->repo);
        $processor->process($response,m::mock(\POSD\Persistence\Payment::class));

    }

    /**
     * @test
     */
    public function it_does_not_do_anything_on_500()
    {
        $stream = Psr7\stream_for('{"data" : "test"}');
        $response = new GuzzleHttp\Psr7\Response(500, ['Content-Type' => 'application/json'], $stream);
        $this->repo = m::mock(PaymentRepository::class)->shouldReceive('markInvalid')->times(0)->shouldReceive('markSuccess')->times(0)->getMock();

        $processor = new \POSD\Handlers\ResponseProcessor($this->repo);
        $processor->process($response,m::mock(\POSD\Persistence\Payment::class));

    }

}
