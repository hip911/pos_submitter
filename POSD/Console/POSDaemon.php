<?php

namespace POSD\Console;

use Illuminate\Console\Command;
use POSD\Handlers\PaymentSubmitter;
use POSD\Handlers\ResponseProcessor;
use POSD\Persistence\PaymentRepository;

class POSDaemon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posdaemon:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listens for new entries in the DB.';

    protected $paymentRepository;

    protected $submitter;

    protected $processor;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PaymentRepository $paymentRepository,PaymentSubmitter $submitter,ResponseProcessor $processor)
    {
        parent::__construct();
        $this->payment = $paymentRepository;
        $this->submitter = $submitter;
        $this->processor = $processor;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('waiting for entries...');
        while(true){

            $submittablePayments = $this->payment->getSubmittable();
            foreach($submittablePayments as $payment)
            {
                $response = $this->submitter->submit($payment);
                $this->processor->process($response,$payment);
            }
            sleep(1); // optional
            $this->info(count($submittablePayments).' entries processed ... trying again ... ');
        }
    }
}
