<?php
/**
 * Created by PhpStorm.
 * User: hip
 * Date: 20/08/15
 * Time: 16:48
 */

namespace POSD\Handlers;

use GuzzleHttp\Client as GuzzleClient;
use POSD\Persistence\Payment;


class GuzzlePaymentSubmitter implements PaymentSubmitter {

    protected $endpoint = 'http://178.62.89.122:8081/v1/payments';

    protected $guzzle;

    public function __construct(GuzzleClient $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    public function submit(Payment $payment)
    {
        $paymentPostData = $this->formatPostData($payment);
        $response = $this->guzzle->post($this->endpoint,['json' => $paymentPostData ]);
        return $response;
    }

    private function formatPostData(Payment $payment)
    {
        return [
            'auth_key'      => $payment->location->auth_key,
            'payment' => [
                'amount'    => floatval($payment->amount),
                'reference' => $payment->id,
                'card_type' => intval($payment->cardtype->id),
                'table'     => intval($payment->table_number)
            ]
        ];
    }
}