<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use POSD\Handlers\GuzzlePaymentSubmitter;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('POSD\Handlers\PaymentSubmitter', function () {
            return new GuzzlePaymentSubmitter(new Client());
        });
    }
}
