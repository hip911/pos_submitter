<?php

namespace POSD\Persistence;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['user_id','amount','cardtype_id','location_id','table_number','success','invalid'];

    protected $with = ['cardtype','location'];

    public function cardtype()
    {
        return $this->belongsTo('POSD\Persistence\Cardtype');
    }

    public function location()
    {
        return $this->belongsTo('POSD\Persistence\Location');
    }

    public function scopeReadyToSubmit($query)
    {
        return $query->where(['success'=>0,'invalid'=>0]);
    }
}
