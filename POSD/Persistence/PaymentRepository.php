<?php
/**
 * Created by PhpStorm.
 * User: hip
 * Date: 20/08/15
 * Time: 17:30
 */

namespace POSD\Persistence;


class PaymentRepository {

    protected $model;

    public function __construct(Payment $model)
    {
        $this->model = $model;
    }

    public function getSubmittable()
    {
        return $this->model->readyToSubmit()->get();
    }

    public function markSuccess(Payment $payment)
    {
        $this->model->find($payment->id)->update(['success'=>1]);
    }

    public function markInvalid(Payment $payment)
    {
        $this->model->find($payment->id)->update(['invalid'=>1]);
    }

}