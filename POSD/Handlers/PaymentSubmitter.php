<?php

namespace POSD\Handlers;

/**
 * Created by PhpStorm.
 * User: hip
 * Date: 20/08/15
 * Time: 16:47
 */

use POSD\Persistence\Payment;

interface PaymentSubmitter {

    public function submit(Payment $payment);

}