<?php

namespace POSD\Persistence;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ['name','auth_key'];

    public $timestamps = false;
}
