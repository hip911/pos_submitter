<?php
/**
 * Created by PhpStorm.
 * User: hip
 * Date: 20/08/15
 * Time: 19:21
 */

namespace POSD\Handlers;


use GuzzleHttp\Psr7\Response;
use POSD\Persistence\Payment;
use POSD\Persistence\PaymentRepository;

class ResponseProcessor {

    protected $response;

    protected $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    public function process(Response $response,Payment $payment)
    {
        switch($response->getStatusCode())
        {
            case 200:
                $this->paymentRepository->markSuccess($payment);
                break;
            case 400:
                $this->paymentRepository->markInvalid($payment);
                break;
            case 500:
            default:
                break;
        }
    }

}