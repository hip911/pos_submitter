<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);

        DB::table('cardtypes')->truncate();
        DB::table('locations')->truncate();
        DB::table('payments')->truncate();

        /* ADD CARD TYPES */
        \POSD\Persistence\Cardtype::create([
            'name' => 'Visa'
        ]);
        \POSD\Persistence\Cardtype::create([
            'name' => 'MasterCard'
        ]);
        \POSD\Persistence\Cardtype::create([
            'name' => 'American Express'
        ]);

        /* ADD LOCATIONS */
        \POSD\Persistence\Location::create([
            'name' => 'London Fleet Street',
            'auth_key' => '84dd44e9ad611789fe9ff16a225f10b3'
        ]);
        \POSD\Persistence\Location::create([
            'name' => 'London Angel Islington',
            'auth_key' => '50d20956c7fdc07160fbdae3847aba50'
        ]);

        /* ADD PAYMENTS */
        $paymentData = [
            [
                'user_id' => 14424,
                'amount' => 10.63,
                'cardtype_id' => 1,
                'location_id' => 1,
                'table_number' => 12,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 16:14'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 16:14')
            ],
            [
                'user_id' => 55353,
                'amount' => 52.99,
                'cardtype_id' => 3,
                'location_id' => 1,
                'table_number' => 2,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 16:23'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 16:23')
            ],
            [
                'user_id' => 34111,
                'amount' => 1.08,
                'cardtype_id' => 1,
                'location_id' => 2,
                'table_number' => 41,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 16:38'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 16:38')
            ],
            [
                'user_id' => 45625,
                'amount' => 13.12,
                'cardtype_id' => 2,
                'location_id' => 1,
                'table_number' => 99,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 16:55'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 16:55')
            ],
            [
                'user_id' => 37127,
                'amount' => 143.20,
                'cardtype_id' => 1,
                'location_id' => 2,
                'table_number' => 13,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 16:56'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 16:56')
            ],
            [
                'user_id' => 62667,
                'amount' => 23.45,
                'cardtype_id' => 1,
                'location_id' => 1,
                'table_number' => 102,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 17:12'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 17:12')
            ],
            [
                'user_id' => 2344,
                'amount' => 19.99,
                'cardtype_id' => 2,
                'location_id' => 2,
                'table_number' => 66,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 18:14'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 18:14')
            ],
            [
                'user_id' => 33457,
                'amount' => 14.49,
                'cardtype_id' => 1,
                'location_id' => 2,
                'table_number' => 66,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 18:14'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 18:14')
            ],
            [
                'user_id' => 27776,
                'amount' => 233.20,
                'cardtype_id' => 3,
                'location_id' => 1,
                'table_number' => 25,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 18:22'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 18:22')
            ],
            [
                'user_id' => 35723,
                'amount' => 94.77,
                'cardtype_id' => 2,
                'location_id' => 2,
                'table_number' => 71,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 18:35'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 18:35')
            ],
            [
                'user_id' => 63667,
                'amount' => 47.94,
                'cardtype_id' => 1,
                'location_id' => 1,
                'table_number' => 88,
                'created_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 18:49'),
                'updated_at' => DateTime::createFromFormat('d/m/Y H:i','16/02/2015 18:49')
            ]

        ];
        foreach ($paymentData as $entry)
        {
            \POSD\Persistence\Payment::create($entry);
        }


        Model::reguard();
    }
}
