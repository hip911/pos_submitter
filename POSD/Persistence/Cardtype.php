<?php

namespace POSD\Persistence;

use Illuminate\Database\Eloquent\Model;

class Cardtype extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;


}
